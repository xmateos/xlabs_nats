<?php

namespace XLabs\NatsBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use XLabs\NatsBundle\Services\Firewall;
use XLabs\NatsBundle\Services\MailNotification;

use XLabs\NatsBundle\Request\Request as NatsRequest;
use XLabs\NatsBundle\Event\OnActivate;
use XLabs\NatsBundle\Event\OnAdd;
use XLabs\NatsBundle\Event\OnChange;
use XLabs\NatsBundle\Event\OnCheck;
use XLabs\NatsBundle\Event\OnDelete;
use XLabs\NatsBundle\Event\OnExpire;
use XLabs\NatsBundle\Event\OnManualAdd;
use XLabs\NatsBundle\Event\OnTrialToFull;
use XLabs\NatsBundle\Event\OnRebill;

use Symfony\Component\HttpFoundation\Response;

class Postback
{
    private $config;
    private $event_dispatcher;
    private $firewall;
    private $mail_notification;

    public function __construct($config, $event_dispatcher, Firewall $firewall, MailNotification $mail_notification)
    {
        /*
         * 'event_dispatcher' gets different interfaces depending on the environment, that´s why I don´t set its type
         */
        $this->config = $config;
        $this->event_dispatcher = $event_dispatcher;
        $this->firewall = $firewall;
        $this->mail_notification = $mail_notification;
    }
    
    public function onNatsPostback(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if($request->get('_route') == 'xlabs_nats_postback')
        {
            $querystring = $request->getQueryString();

            // Notification
            /*$nSubject = "Trying postback";
            $nBody = '<a href="http://www.bikinifanatics.com/nats/postback?'.$querystring.'" target="_blank">Resend postback</a><br /><br />'.$querystring;
            $nHeaders  = 'MIME-Version: 1.0' . "\r\n";
            $nHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            mail("xavi.mateos@manicamedia.com", $nSubject, $nBody, $nHeaders);*/

            $natsRequest = new NatsRequest($querystring);

            $status_code = 200;
            if(!$this->firewall->natsIpAllowed())
            {
                $content = 'IP restricted';
                $status_code = 403;
            } else {
                $e = false;
                $send_notification = true;
                switch($natsRequest->__get('action'))
                {
                    case "ADD":
                        $e = new OnAdd($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "MANUALADD":
                        $e = new OnManualAdd($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "ACTIVATE":
                        // Expiration date for this postback is wrong. That´s why we set the "rebill" postback URL explicitly
                        $e = new OnActivate($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "TRIALTOFULL":
                        $e = new OnTrialToFull($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "DELETE":
                        $e = new OnDelete($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "CHANGE":
                        $e = new OnChange($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "EXPIRE":
                        $e = new OnExpire($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        break;
                    case "CHECK":
                        $e = new OnCheck($natsRequest->getData());
                        $this->event_dispatcher->dispatch($e::NAME, $e);
                        $response = $e->getResponse();
                        $send_notification = false;
                        break;
                    default:
                        switch($natsRequest->__get('post_type'))
                        {
                            case 'rebillpost':
                                $e = new OnRebill($natsRequest->getData());
                                $this->event_dispatcher->dispatch($e::NAME, $e);
                                $response = $e->getResponse();
                                break;
                            default:
                                $response = 'Wrong postback type';
                                $send_notification = false;
                                break;
                        }
                        break;
                }

                if($send_notification)
                {
                    $this->mail_notification->send($natsRequest);
                }

                $content = $response ? $response : 'OK'; // force 'OK' response if non is given
            }

            $response = new Response();
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);
            $response->setContent($content);
            $response->setStatusCode($status_code);
            $event->setResponse($response);
        }
    }
}