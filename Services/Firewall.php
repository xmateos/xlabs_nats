<?php

namespace XLabs\NatsBundle\Services;

class Firewall
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function ip_in_range($ip, $range)
    {
        if(strpos($range, '/') == false)
        {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list($range, $netmask) = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
    }

    public function natsIpAllowed($ip = false)
    {
        $ip = $ip ? trim($ip) : trim($_SERVER['REMOTE_ADDR']);
        $allowed_ips = array($_SERVER['SERVER_ADDR']);
        $allowed_ips = array_merge($allowed_ips, $this->config['allowed_ips']);
        if(in_array($ip, $allowed_ips))
        {
            return true;
        } else {
            $allowed_cidr_blocks = array_filter($allowed_ips, function($v){
                if(strpos($v, '/'))
                {
                    return $v;
                }
            });
            foreach(array_filter($allowed_cidr_blocks) as $allowed_cidr_block)
            {
                if($this->ip_in_range($ip, $allowed_cidr_block))
                {
                    return true;
                }
            }
        }
        return false;
    }
}