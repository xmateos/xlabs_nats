<?php

namespace XLabs\NatsBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use XLabs\NatsBundle\Request\Request as NatsRequest;

class MailNotification
{
    private $request;
    private $config;

    public function __construct(RequestStack $request_stack, $config)
    {
        $this->request = $request_stack->getCurrentRequest();
        $this->config = $config;
    }

    public function send(NatsRequest $natsRequest, $subject = false)
    {
        $aData = $natsRequest->getData();
        $querystring = $natsRequest->getQuerystring();

        if(array_key_exists('mail_notifications', $this->config) && $this->config['mail_notifications']['enabled'])
        {
            $url = $this->request->getScheme().'://'.$this->request->headers->get('host');
            $postback_url = $url.$this->config['postback_url'].'?'.$querystring;

            // Subject
            $nSubject = 'Nats postback: ';
            $nSubject .= $this->config['mail_notifications']['subject'] ? $this->config['mail_notifications']['subject'].' ' : '';
            $nSubject .= $subject ? $subject : '';
            $postback_action = array_key_exists('action', $aData) ? $aData['action'] : false;
            $postback_action = $postback_action ? $postback_action : (array_key_exists('post_type', $aData) ? $aData['post_type'] : false);
            $nSubject .= $postback_action ? ' '.strtoupper($postback_action) : '';
            $nSubject .= array_key_exists('username', $aData) ? ' '.$aData['username'] : '';

            // Body
            $nBody = '<br /><span style="text-align: center; background-color: #4DB24C; padding: 10px 15px; border-radius: 5px;"><a href="'.$postback_url.'" style="color: #fff; font-size: 14px; letter-spacing: 1px; text-decoration: none; text-shadow: 0px 2px 2px #4db24c; font-family: Arial, Helvetica, sans-serif;">Click me to resend postback</a></span><br /><br />';
            //$nBody .= '<a href="'.$postback_url.'" target="_blank">Resend postback</a><br /><br />';
            if(is_array($aData) && count($aData))
            {
                $nBody .= '<br /><b>Parameters</b>:<br />';
                $this->printArray($aData, $nBody);
                /*foreach($aData as $key => $value)
                {
                    //$nBody .= $key.' <b>-></b> <i>'.$value.'</i><br />';
                    dump($value);
                    $nBody .= '<span style="margin-left: 30px;">'.$key.': <i>'.$value.'</i><span/><br />';
                }*/
            }
            $nBody .= '<br /><br /><b>Querystring</b>:<br />'.$querystring;

            // Headers
            $nHeaders  = 'MIME-Version: 1.0' . "\r\n";
            $nHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            if($this->config['mail_notifications']['from'] != '')
            {
                $nHeaders .= 'From: '.$this->config['mail_notifications']['from'] . "\r\n";
            }

            if(count($this->config['mail_notifications']['destinataries']))
            {
                foreach($this->config['mail_notifications']['destinataries'] as $recipient)
                {
                    mail($recipient, $nSubject, $nBody, $nHeaders);
                }
            }
        }
    }

    private function printArray($a, &$body, $parent_key = false)
    {
        foreach($a as $key => $value)
        {
            if(is_array($value) && count($value))
            {
                return $this->printArray($value, $body, $key);
            } else {
                $key = $parent_key ? $parent_key.'['.$key.']' : $key;
                $body .= '<span style="margin-left: 30px;">'.$key.': <i>'.$value.'</i><span/><br />';
            }
        }
    }
}