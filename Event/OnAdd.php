<?php

namespace XLabs\NatsBundle\Event;

class OnAdd extends Postback
{
    const NAME = 'nats_postback.OnAdd.event';
}