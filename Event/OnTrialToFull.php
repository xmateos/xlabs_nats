<?php

namespace XLabs\NatsBundle\Event;

class OnTrialToFull extends Postback
{
    const NAME = 'nats_postback.OnTrialToFull.event';
}