<?php

namespace XLabs\NatsBundle\Event;

class OnCheck extends Postback
{
    const NAME = 'nats_postback.OnCheck.event';
}