<?php

namespace XLabs\NatsBundle\Event;

class OnExpire extends Postback
{
    const NAME = 'nats_postback.OnExpire.event';
}