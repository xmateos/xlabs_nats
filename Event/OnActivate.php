<?php

namespace XLabs\NatsBundle\Event;

class OnActivate extends Postback
{
    const NAME = 'nats_postback.OnActivate.event';
}