<?php

namespace XLabs\NatsBundle\Event;

class OnManualAdd extends Postback
{
    const NAME = 'nats_postback.OnManualAdd.event';
}