<?php

namespace XLabs\NatsBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Postback extends Event
{
    const NAME = 'xlabs_nats_postback.event';

    protected $params;
    protected $response;

    public function __construct($params)
    {
        $this->params = $params;
        $this->params['___x_params'] = array(); // custom array to let later listeners know what kind of event was (membership, tokens, etc)
        $this->response = false;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($name)
    {
        return array_key_exists($name, $this->params) ? $this->params[$name] : false;
    }

    public function setXParam($name, $value)
    {
        $this->params['___x_params'][$name] = $value;
        return $this->params;
    }

    public function getXParam($name)
    {
        return array_key_exists($name, $this->params['___x_params']) ? $this->params['___x_params'][$name] : false;
    }

    public function getQuerystring()
    {
        return http_build_query($this->getParams());
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }
}