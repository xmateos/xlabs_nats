<?php

namespace XLabs\NatsBundle\Event;

class OnChange extends Postback
{
    const NAME = 'nats_postback.OnChange.event';
}