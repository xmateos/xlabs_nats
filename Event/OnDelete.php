<?php

namespace XLabs\NatsBundle\Event;

class OnDelete extends Postback
{
    const NAME = 'nats_postback.OnDelete.event';
}