<?php

namespace XLabs\EpochBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * @ORM\Table(name="NatsTransaction")
 * @ORM\Entity
 */
class NatsTransaction
{
    /**
     * @ORM\Column(name="transaction_id", type="bigint")
     * @ORM\Id
     */
    private $transaction_id;

    public function __construct()
    {

    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}

