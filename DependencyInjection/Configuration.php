<?php

namespace XLabs\NatsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_nats');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->scalarNode('postback_url')->defaultValue('/nats/postback')->end()
                ->arrayNode('allowed_ips')->prototype('scalar')->defaultValue('[]')->end()->end()
                ->arrayNode('api')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('url')->defaultValue('https://manicamoney.com/api')->end()
                        ->scalarNode('username')->defaultValue('manicaxavi')->end()
                        ->scalarNode('key')->defaultValue('a5b542c3f93dd75579cc4f22014f84bc')->end()
                    ->end()
                ->end()
                ->arrayNode('mail_notifications')->addDefaultsIfNotSet()
                    ->treatFalseLike(array('enabled' => false))
                    ->treatTrueLike(array('enabled' => true))
                    ->treatNullLike(array('enabled' => false))
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('subject')->defaultValue('')->end()
                        ->scalarNode('from')->defaultValue('')->end()
                        ->arrayNode('destinataries')->prototype('scalar')->defaultValue('[]')->end()->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
