<?php

namespace XLabs\NatsBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class NatsRoutingLoader extends Loader
{
    private $config;
    private $loaded = false;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "xlabs_nats_routing" loader twice');
        }

        $routes = new RouteCollection();

        // prepare a new route
        $path = $this->config['postback_url'];
        $defaults = array(
            '_controller' => 'XLabsNatsBundle:Nats:postback',
        );
        /*$requirements = array(
            'parameter' => '\d+',
        );*/
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);

        // add the new route to the route collection
        $routeName = 'xlabs_nats_postback';
        $routes->add($routeName, $route);

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'xlabs_nats_routing' === $type;
    }
}