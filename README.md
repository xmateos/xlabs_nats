A NATS postback wrapper.

## Installation ##
Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/natsbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\NatsBundle\XLabsNatsBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_nats:
    resource: .
    type: xlabs_nats_routing
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_nats:
    postback_url: /your/nats/postback/url
    allowed_ips: ['127.0.0.1', '192.168.1.10', '60.58.43.125', ...]
    api:
        url: https://<nats_domain>/api
        username: USERNAME
        key: APIKEY
```
Note: $_SERVER['SERVER_ADDR'] value is allowed by default, so no need to include it in the 'allowed_ips' array.

### Event listeners ###
The following events take place when a nats postback is reached:

- nats_postback.OnAdd.event (ADD)  
- nats_postback.OnManualAdd.event (MANUALADD)  
- nats_postback.OnActivate.event (ACTIVATE)  
- nats_postback.OnChange.event (CHANGE)  
- nats_postback.OnCheck.event (CHECK)  
- nats_postback.OnDelete.event (DELETE)  
- nats_postback.OnExpire.event (EXPIRE)  
- nats_postback.OnTrialToFull.event (TRIALTOFULL)
- nats_postback.OnRebill.event (REBILL POST)

If you want to register an event listener for any of them:
``` yml
# YourBundle/Resources/config/services.yml
    ...
    custom_listener_for_nats_onAdd.event_listener:
        class:  YourBundle\EventListener\MyListener
        tags:
            - { name: kernel.event_listener, event: nats_postback.OnAdd.event, method: onAdd }
```
```php
namespace YourBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;

class MyListener extends Event
{
    public function onAdd(Event $event)
    {
        $params = $event->getParams(); // all params sent by nats
        
        ...

        /* Dont forget to set the response, based on Nats documentation:
            http://tmmwiki.com/index.php/NATS4_Site_User_Management
            OK|message
            NOTOK|message
            ERROR|message
        */
        $event->setResponse('OK');
    }
}
```
If no response is set in the listener, 'OK' will be the default output.