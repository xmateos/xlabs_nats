<?php

namespace XLabs\NatsBundle\Request;

class Request
{
    private $querystring;
    private $data;

    public function __construct($querystring)
    {
        $this->querystring = $querystring;
        parse_str($this->querystring, $this->data);
    }

    public function __get($name)
    {
        if(array_key_exists($name, $this->data))
        {
            return $this->data[$name];
        }
        return false;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getQuerystring()
    {
        return $this->querystring;
    }
}

